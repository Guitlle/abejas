 
axs = "se4thse534y6u57dhs4g34657udjtnhxrb546r78ikyfjtdnh54";
BrowserState = {
    changedViewport: false
};
class Browser {
    static init() {
        screen.lockOrientationUniversal = screen.lockOrientation || screen.mozLockOrientation || screen.msLockOrientation;
        if (screen.lockOrientationUniversal) {
            screen.lockOrientationUniversal("portrait");
        }
        window.addEventListener("resize", () => { Browser.resize(); });
    }
    
    static resize() {
        var fixedHeight = 900;
        var fixedWidth = 675;
        var windowWidth = window.innerWidth;
        var windowHeight = window.innerHeight;
        var windowRatio = windowWidth / windowHeight;
        
        var canvas = document.querySelector("canvas");
        var gameGui = document.getElementById("game-gui-output");
        var gameRatio = game.config.width / game.config.height;
        var viewport = document.querySelector("meta[name=viewport]");
        
        if  (windowRatio < gameRatio){
            viewport.setAttribute('content', 'width=' + fixedWidth + ',user-scalable=0"');
        }
        else {
            viewport.setAttribute('content', 'width='+Math.round(windowRatio*fixedHeight)+',user-scalable=0"');
        }
        canvas.style.width  = fixedWidth  + "px";
        canvas.style.height = fixedHeight + "px";
        canvas.parentNode.style.left = (windowWidth - fixedWidth)/2 + "px";
        canvas.parentNode.style.top = (windowHeight - fixedHeight)/2 + "px";
        app.style.width  = fixedWidth;
        app.style.height = fixedHeight;
        app.style.left = (windowWidth - fixedWidth)/2;
        app.style.top = (windowHeight - fixedHeight)/2;
        
    }

    static wait(ms) {
        return new Promise((r, j)=>setTimeout(r, ms));
    }

}

var itemCount = 0;
class Item {
    
    static get rowHeight() { return 57; }
    static get itemsOptions() { return [
                    "florA", 
                    "florB", 
                    "florC", 
                    "florNormal1",
                    "florNormal2",
                    "florNormal3",
                    /*"florD",*/ 
                    "tomateT", 
                    "maizT",
                    "bomba",
                    "frozen"
                ];
            }

    
    constructor (scene, x, y, cat, tipoItem, n) {
        itemCount ++;
        var textColor = "#ffffff";
        this.initialN = n;
        this.n = n;
        this.id = itemCount;
        this.sprite = scene.matter.add.sprite(x, y, tipoItem + "1", null, { isStatic: true });
        this.sprite.setDepth(5);
        this.sprite.name = "item";
        this.textOffset = new Phaser.Math.Vector2(0,0);
        if ( (["florA", "florB", "florC"]).indexOf(tipoItem) != -1 )  {
            this.sprite.setBody({
                "type": "fromVertices", verts: [
                            {x : 27, y : 8},
                            {x : 77, y : 8},
                            {x : 100, y : 55},
                            {x : 77, y : 100},
                            {x : 27, y : 100},
                            {x : 3, y : 55}
                    ] });
            this.contorno = scene.add.image(x,y, "contorno_hex");
            this.contorno.setScale(0.60).setDepth(4);
            this.contorno_danger = scene.add.image(x,y, "contorno_hex_red");
            this.contorno_danger.setScale(0.60).setDepth(4);
            this.sprite.anims.play(tipoItem);
        }
        else if (tipoItem == "tomateT") {
            this.sprite.setBody({
                "type": "fromVertices", verts: [
                            {x : 52, y : 8},
                            {x : 100, y : 55},
                            {x : 52, y : 100},
                            {x : 3, y : 55}
                    ] });
            this.textOffset.y = -20;
            this.textOffset.x = -10;
            this.contorno = scene.add.image(x,y, "contorno_rombo");
            this.contorno.setScale(0.60);
            this.contorno.setDepth(4);
            this.contorno_danger = scene.add.image(x,y, "contorno_rombo_red");
            this.contorno_danger.setScale(0.60).setDepth(4);
            this.sprite.anims.play(tipoItem);
        }
        else if ((["florD", "maizT"]).indexOf(tipoItem) != -1) {
            this.sprite.setBody({
                "type": "fromVertices", verts: [
                            {x : 50, y : 0},
                            {x : 100, y : 90},
                            {x : 0, y : 90}
                    ] });
            scene.matter.verts.translate(this.sprite.body.vertices, 
                new Phaser.Math.Vector2(0, 15) ); 
            this.textOffset.x = -5;
            this.contorno = scene.add.image(x,y, "contorno_tri");
            this.contorno.setScale(0.60);
            this.contorno.setDepth(4);
            this.contorno_danger = scene.add.image(x,y, "contorno_tri_red");
            this.contorno_danger.setScale(0.60).setDepth(4);
            this.sprite.anims.play(tipoItem);
        }
        else if (tipoItem == "bomba") {
            this.sprite.setBody({
                "type": "fromVertices", verts: [
                            {x : 17, y : 0},
                            {x : 82, y : 0},
                            {x : 100, y : 65},
                            {x : 50, y : 100},
                            {x : 0, y : 65}
                    ] });
            scene.matter.verts.translate(this.sprite.body.vertices, 
                new Phaser.Math.Vector2(0, -5) ); 
            this.textOffset.x = -3;
            this.textOffset.y = -46;
            this.contorno = scene.add.image(x,y, "contorno_penta");
            this.contorno.setScale(0.60);
            this.contorno.setDepth(4);
            this.contorno_danger = scene.add.image(x,y, "contorno_penta_red");
            this.contorno_danger.setScale(0.60).setDepth(4);
            this.sprite.anims.play(tipoItem);
        }
        else if ( tipoItem == "frozen") {
            this.sprite.setBody({
                type: "fromVertices", 
                verts: [
                            {x : 27, y : 8},
                            {x : 77, y : 8},
                            {x : 100, y : 55},
                            {x : 77, y : 100},
                            {x : 27, y : 100},
                            {x : 3, y : 55}
                    ] 
            });
            this.sprite.setAlpha(0.7);
            textColor = "#222266";
            this.contorno_danger = scene.add.image(x,y, "contorno_hex_red");
            this.contorno_danger.setScale(0.60).setDepth(4);
        }
        else if (["florNormal1", "florNormal2", "florNormal3"].indexOf(tipoItem) != -1) {
            this.sprite.setTexture(tipoItem);
            this.sprite.setBody({
                type: "fromVertices", 
                verts: [
                            {x : 0, y : 0},
                            {x : 100, y : 0},
                            {x : 100, y : 100},
                            {x : 0, y : 100}
                    ] 
            });
            this.contorno = scene.add.image(x,y, "contorno_cuadro");
            this.contorno.setScale(0.60);
            this.contorno.setDepth(4);
            this.contorno_danger = scene.add.image(x,y, "contorno_cuadro_red");
            this.contorno_danger.setScale(0.60).setDepth(4);
            this.n = this.initialN = 1;
            this.textOffset.x = -8;
            this.textOffset.y = 3;
            textColor = "#ffffff";
        }
        this.contorno_danger.setVisible(false);
        this.sprite.setScale(0.60);
        
        this.sprite.setCollisionCategory(cat);
        this.sprite.itemObject = this;
        this.text = scene.add.text(x + this.textOffset.x - 16, y + 15 + this.textOffset.y, 
                                this.n+"", 
                                { 
                                    fontFamily: 'Arial', 
                                    fontSize: 10, 
                                    color: textColor,
                                    fontStyle: "bold"
                                });
        this.text.setDepth(6);
        this.type = tipoItem;
        this.scene = scene;
    }
    
    hit (bee, n) {
        if (n==undefined) n = 1;
        if (this.destroyed) return;
        this.n -= n;
        if (this.n > 0) {
            this.text.setText(this.n+"");
            if (this.type == "frozen") {
            }
            else {
            }
            return true;
        } else {
            if (this.type == "bomba") {
                this.destroy(bee);
            }
            else if (this.type == "frozen") {
                this.destroy();
                var bee = new Bee(this.scene, gameController.catBees, gameController.catItems);
                bee.sprite.setPosition(beesController.beesOrigin.x, beesController.beesOrigin.y)
                beesController.bees.push(bee);
                beesController.updateBeeCounter("+1", "#b6fd15");
                bee.sprite.anims.play("fly");
            }
            else {
                this.destroy();
                audioController.polinizarSound.play();
            }
            return false;
        }
    }
    
    danger () {
        if (this.dangerAnimation) return;
        if (this.contorno_danger) {
            this.contorno_danger.setVisible(true);
            
            if (this.contorno) 
                this.contorno.setAlpha(1.0);

            this.dangerAnimation = this.scene.tweens.add({
                    targets: [this.contorno_danger],
                    scaleX: "+=0.2",
                    scaleY: "+=0.2",
                    duration: 400,
                    ease: 'Sine',
                    repeat: -1,
                    yoyo: true
                });
        }
    }
    
    explode (bee) {
        var x = this.sprite.x, y = this.sprite.y;
        var explosion = this.scene.add.sprite(x, y, "explode1");
        explosion.on('animationcomplete', function () {
            explosion.destroy();
        }, this);
        explosion.anims.play("explode");
        audioController.explosionSound.play();
        if (bee) bee.destroy();
        itemsController.items.forEach((i) => {
            if (i.id == this.id || i.destroyed || !i.sprite) return;
            var dx = x-i.sprite.x, dy = y - i.sprite.y;
            if ( (dx*dx + dy*dy) < 100*100)
                i.destroy();
        });
    }
    
    destroy(bee) {
        if (!this.destroyed) {
            this.destroyed = true;
            if (this.dangerAnimation) {
                this.dangerAnimation.stop();
            }
            if (this.type == "bomba") {
                this.explode(bee);
            }
            this.text.destroy();
            this.sprite.destroy();
            if (this.contorno) this.contorno.destroy();
            if (this.contorno_danger) this.contorno_danger.destroy();
        }
    }
    
    moveDown() {
        if (this.destroyed) return;
        this.sprite.y += Item.rowHeight;
        this.text.y += Item.rowHeight;
        if (this.contorno) this.contorno.y += Item.rowHeight;
        if (this.contorno_danger) this.contorno_danger.y += Item.rowHeight;
        if (this.sprite.y > beesController.beesOrigin.y - 200) 
            this.danger();
        return this.sprite.y > beesController.beesOrigin.y - 40;
    }
}

var nBees = 0;
class Bee {
    
    constructor(scene, catBee, catItem) {
        nBees = beesController.bees.length+1;
        this.sprite = scene.matter.add.sprite(100, 100, "abeja1", null, {})
        this.sprite
            .setScale(0.25)
            .setCircle(0.3*this.sprite.width/4, 0.25*this.sprite.halfWidth, 0.25*this.sprite.halfHeight)
            .setBounce(1)
            .setFriction(0)
            .setFrictionAir(0)
            .setFixedRotation()
            .setRotation(0)
            .setCollisionCategory(catBee)
            .setCollidesWith([1, catItem]);
            
        this.sprite.anims.play("fly");  
        this.sprite.name = "bee";
        this.sprite.beeObject = this;
        this.beeId = nBees;
        this.scene = scene;
    }
    
    reset() {
        this.sprite.setVelocity(0,0);
        this.sprite.setPosition(beesController.beesOrigin.x, beesController.beesOrigin.y);
        this.sprite.setRotation(0);
        if (this.flying) {
            this.flying = false;
            beesController.beesOut--;
            beesController.updateBeesWaiting();
        }
    }
    
    destroy() {
        this.sprite.destroy();
        this.destroyed = true;
        beesController.bees = beesController.bees.filter((b) => { return b.destroyed != true; } );
        beesController.updateBeeCounter("-1", "#BB4444");
        beesController.beesOut--;
        if (beesController.bees.length == 0) {
            gameController.gameOver(this.scene);
        }
    }
    
}

class ControllerRegistry extends Array {
    constructor(name) {
        super();
        this.name = name;     
    }
    register(controller) {
        this.push(controller);
    }
    preload (loader) {
        console.log(this);
        this.forEach( (i) => {
            i.preload(loader);
        } );
    }
    create (scene) {
        this.forEach( (i) => {
            i.create(scene);
        } );
    }
    update (scene) {
        this.forEach( (i) => {
            if (i.update) i.update(scene);
        } );
    }
}

var sceneRegistry = new ControllerRegistry("game scene");

// Controllers are singletons

// Game 
var gameController = {
    level: 0,
    preload: function (loader) {
        gameController.level = 0;
        var progress = loader.add.graphics();
        loader.load.on('progress', function (value) {

            progress.clear();
            progress.fillStyle(0xffffff, 1);
            progress.fillRect(0, 270, 600 * value, 60);

        });

        loader.load.on('complete', function () {
            progress.destroy();
        });
        loader.load.image('fondo', 'assets/fondo.png');
        Browser.resize();
    },
    create: function (scene) {
        scene.matter.world.setBounds();
        gameController.catBees = scene.matter.world.nextCategory(), 
        gameController.catItems = scene.matter.world.nextCategory();
        scene.add.image(0, 0, 'fondo').setOrigin(0,0);
        gameController.levelText = scene.add.text(45, 755, gameController.level+"", 
            { 
                fontFamily: 'Arial', 
                fontStyle: "bold", 
                fontSize: 30, 
                color: '#444444'
            });
        gameController.levelText.setOrigin(0.5,0);    
        gameController.graphics = 
            scene.add.graphics({ lineStyle: { width: 6, color: 0xffffff, alpha: 0.6 } });
        /*
        var piedra1 = this.matter.add.fromVertices(75, 60, [
                    {x : 0, y : 0},
                    {x : 90, y : 0},
                    {x : 90, y : 60},
                    {x : 0, y : 60}
                    
                ], { isStatic: true });
        var piedra2 = this.matter.add.fromVertices(535, 200, [
                    {x : 0, y : 0},
                    {x : 100, y : 0},
                    {x : 100, y : 80},
                    {x : 0, y : 80}
                    
                ], { isStatic: true });
        var piedra3 = this.matter.add.fromVertices(50, 340, [
                    {x : 0, y : 0},
                    {x : 80, y : 0},
                    {x : 80, y : 60},
                    {x : 0, y : 60}
                    
                ], { isStatic: true });
        var piedra4 = this.matter.add.fromVertices(560, 680, [
                    {x : 0, y : 0},
                    {x : 90, y : 0},
                    {x : 90, y : 80},
                    {x : 0, y : 80}
                    
                ], { isStatic: true });
        */
    },
    gameOver: function (scene) {
        beesController.uiStatus = "gameover";
        scene.scene.pause();
        /*gameController.gameOverGraphics = scene.add.graphics();
        gameController.gameOverGraphics.setDepth(10);
        gameController.gameOverGraphics.fillStyle(0xefd025, 1);
        gameController.gameOverGraphics.fillRect(0, 300, 600, 200);
        gameController.gameOverText = scene.add.text(300, 350, "GAME OVER", { fontStyle: "bold", fontFamily: 'Arial', fontSize: 40, 
                    shadow: {
                            color: 'black',
                            offsetX: 2,
                            offsetY: 2,
                            blur: 2
                    }, 
                    padding: {
                        left: 20,
                        right: 20,
                        top: 20,
                        bottom: 20
                        //x: 32,    // 32px padding on the left/right
                        //y: 16     // 16px padding on the top/bottom
                    },
                    color: '#444444',         
                    backgroundColor: "#efd025" });
        gameController.gameOverText.setOrigin(0.5, 0);
        gameController.gameOverText.setDepth(11);*/
        app.gameOver = true;
    }


}
sceneRegistry.register(gameController);

// Audio
var audioController = {
    preload: function (loader) {
        loader.load.audio('flight', [
            'assets/sonidos/flight_of_the_bumblebee_2.mp3'
        ]);
        loader.load.audio('bees', [
            'assets/sonidos/Bee-SoundBible.com-914644763_B.mp3'
        ]);
        loader.load.audio('polinizar', [
            'assets/sonidos/polinizar.mp3'
        ]);
        loader.load.audio('click', [
            'assets/sonidos/click.mp3'
        ]);
        loader.load.audio('explosion', [
            'assets/sonidos/explosion_test.mp3'
        ]);
    },
    create: function (scene) {
        var that = audioController;
        that.flightSong = scene.sound.add('flight');
        that.beesSound = scene.sound.add('bees');
        that.polinizarSound = scene.sound.add('polinizar');
        that.clickSound = scene.sound.add('click');
        that.clickSound.setVolume(0.4);
        that.beesSound.setVolume(0.4);
        that.flightSong.setVolume(0.45);
        that.beesSound.play();
        that.flightSong.play({loop: true});
        that.explosionSound = scene.sound.add('explosion');
        that.explosionSound.setVolume(0.5);
    }
};
sceneRegistry.register(audioController);

// Abejas 
var beesController = {
    bees: [],
    uiStatus: "play",
    uiThrowLineShow: false,
    beeSpeed: 15,
    initialN: 25,
    beesOut: 0,
    beesOrigin:  {x: 303, y: 740},
    preload: function (loader) {
        delete beesController.bees;
        beesController.bees = [];
        beesController.beesOut = 0;
        loader.load.image('abeja1', 'assets/abejita-1.png');
        loader.load.image('abeja2', 'assets/abejita-2.png');
    },
    create: function (scene) {
        scene.anims.create({
            key: 'fly',
            frames: [ { key: "abeja1", frame: null, duration:4 }, { key: "abeja2", frame: null, duration:5 } ],
            frameRate: 15,
            repeat: -1
        });
        for (var i = 0; i< beesController.initialN; i++) {
            var bee = new Bee(scene, gameController.catBees, gameController.catItems);
            bee.sprite.setPosition(beesController.beesOrigin.x, beesController.beesOrigin.y)
            beesController.bees.push(bee);
        }
        beesController.throwLine  = new Phaser.Geom.Line(
                beesController.beesOrigin.x, beesController.beesOrigin.y, 
                beesController.beesOrigin.x, beesController.beesOrigin.y);
        scene.input.on('pointerdown', beesController.mousedown, scene);
        scene.input.on('pointerup', beesController.mouseup, scene);
        scene.input.on('pointermove', beesController.mousemove, scene);
        scene.matter.world.on('collisionend', beesController.beeCollisions);
        beesController.endThrow = beesController.endThrow.bind(null, scene);
        // watchdog for the bees
        beesController.timer = scene.time.addEvent({ delay: 3000, loop: true,
            callback: () => {
                beesController.bees.forEach((bee) => {
                    if (bee.sprite.x > gameConfig.with || bee.sprite.x < 0 || 
                            bee.sprite.y > gameConfig.height || bee.sprite.y < 0 || bee.flying==false) {
                        bee.reset();
                    }
                    if (bee.flying) {
                        var veloVector = (new Phaser.Math.Vector2(bee.sprite.body.velocity.x, bee.sprite.body.velocity.y));
                        var lengthV = veloVector.length();
                        if (lengthV < beesController.beeSpeed || lengthV > beesController.beeSpeed + 1) {
                            veloVector.scale(beesController.beeSpeed/lengthV);
                            bee.sprite.setVelocityY(veloVector.y);
                            bee.sprite.setVelocityX(veloVector.x);
                        }
                    }
                });
            }
        });
        beesController.beesWaiting = scene.add.text(beesController.beesOrigin.x, beesController.beesOrigin.y+50, 
                                String(beesController.bees.length - beesController.beesOut), 
                                { 
                                    fontFamily: 'Arial', 
                                    fontSize: 15,
                                    fontStyle: "bold",
                                    color: '#444444' 
                                });
        beesController.beesWaiting.setOrigin(0.5,0.5);    
        
        var beeIndicator = scene.add.image(515, 775, "abeja1", {});
        beeIndicator.setScale(0.2);
        beesController.beesCounter = scene.add.text(528, 765, 
                                "" + nBees, 
                                { 
                                    fontFamily: 'Arial', 
                                    fontSize: 16,
                                    fontStyle: "bold",
                                    color: '#444444' 
                                });
        beesController.beesCounterMessage = scene.add.text(520, 750, 
                                "", 
                                { 
                                    fontFamily: 'Arial', 
                                    fontSize: 40,
                                    fontStyle: "bold",
                                    color: '#ffffff',
                                    alpha: 0
                                });
        beesController.beesCounterTween = scene.tweens.add({
                targets: [beesController.beesCounter, beeIndicator],
                scaleX: "+=0.2",
                scaleY: "+=0.2",
                duration: 200,
                ease: 'Sine',
                repeat: 3,
                yoyo: true
            });
        beesController.beesCounterMsgTween = scene.tweens.add({
                targets: [beesController.beesCounterMessage],
                y: 500,
                alpha: 0,
                duration: 3500,
                ease: 'Sine',
                repeat: 0,
                onComplete: () => {
                    beesController.beesCounterMessage.y = 750;
                },
                paused: true
            });
    },
    updateBeesWaiting: function () {
        beesController.beesWaiting.setText(String(beesController.bees.length - beesController.beesOut));
    },
    updateBeeCounter: function (message, color) {
        beesController.beesCounter.setText(beesController.bees.length);
        if (!beesController.beesCounterTween.isPlaying())
            beesController.beesCounterTween.restart();
        if (message) {
            beesController.beesCounterMessage.alpha = 1;
            beesController.beesCounterMessage.setText(message).setColor(color);
            beesController.beesCounterMsgTween.restart();
        }
    },
    drawThrowLine: function (x, y) {
        const __R = 1000;
        var dx = (beesController.throwLine.x1 - x), 
            dy = (beesController.throwLine.y1 - y), 
            r = Math.sqrt(dx*dx + dy*dy);
        beesController.throwLine.x2 = beesController.throwLine.x1 - dx * __R/r;
        beesController.throwLine.y2 = beesController.throwLine.y1 - dy * __R/r;
        gameController.graphics.clear();
        gameController.graphics.strokeLineShape(beesController.throwLine);
    },
    throwBees: async function () {
        beesController.uiStatus = "throwing";
        var dirVec = (new Phaser.Math.Vector2(beesController.throwLine.x2-beesController.throwLine.x1, 
                                              beesController.throwLine.y2-beesController.throwLine.y1)).normalize();
        for(var i = 0; i < beesController.bees.length; i++)
        {
            if (!beesController.bees[i].flying) {
                beesController.bees[i].flying = true;
                beesController.beesOut ++;
                beesController.updateBeesWaiting();
                beesController.bees[i].sprite.setVelocity(beesController.beeSpeed * dirVec.x, beesController.beeSpeed * dirVec.y);
                beesController.bees[i].sprite.setRotation(dirVec.angle() + 1.57);
                await Browser.wait(100);
            }
        }
        beesController.uiStatus = "play";
        itemsController.newRow();
    },
    endThrow: function (scene) {
        audioController.beesSound.__reset = true;
        scene.tweens.add({
                targets: audioController.beesSound,
                volume: 0,

                ease: 'Linear',
                duration: 1000,

                onComplete: function () {
                        if (audioController.beesSound.__reset) 
                            audioController.beesSound.stop(); 
                        audioController.beesSound.setVolume(0.4);
                    }
        });
    },
    mousedown: function (pointer, gameObjects) {
        if (beesController.uiStatus=="play" && !beesController.uiThrowLineShow) {
            beesController.uiThrowLineShow = true;
            beesController.drawThrowLine(pointer.x, pointer.y);
        }
    },
    mouseup: function (pointer, gameObjects) {
        if (beesController.uiStatus=="play" && beesController.uiThrowLineShow) {
            beesController.uiThrowLineShow = false;
            gameController.graphics.clear();
            audioController.beesSound.__reset = false;
            audioController.beesSound.play({loop: true});
            beesController.throwBees();
        }
    },
    mousemove: function (pointer) {
        if (beesController.uiStatus=="play" && beesController.uiThrowLineShow) {
            beesController.drawThrowLine(pointer.x, pointer.y);
        }
    },
    beeCollisions: function (event) {
        event.pairs.forEach(function (pair) {
            var _bee = null;
            if (pair.bodyA.gameObject && pair.bodyA.gameObject.name == "bee") {
                _bee = pair.bodyA.gameObject;
            }
            else if (pair.bodyB.gameObject && pair.bodyB.gameObject.name == "bee") {
                _bee = pair.bodyB.gameObject;
            }
            
            var _item = null;
            if (pair.bodyA.gameObject && pair.bodyA.gameObject.name == "item") {
                _item = pair.bodyA.gameObject;
            }
            else if (pair.bodyB.gameObject && pair.bodyB.gameObject.name == "item") {
                _item = pair.bodyB.gameObject;
            }
            
            if (_bee) {
                if (_bee.y >= beesController.beesOrigin.y) {
                    _bee.beeObject.reset();
                    if (beesController.beesOut == 0) {
                        beesController.endThrow();
                    }
                }
                else {
                    var veloVector = (new Phaser.Math.Vector2(_bee.body.velocity.x, _bee.body.velocity.y));
                    Phaser.Math.Rotate(veloVector, (Phaser.Math.RND.rnd() - 0.5)*0.1 );
                    var angle      = veloVector.angle();
                    _bee.setRotation(angle + 1.57);
                    var lengthV = veloVector.length();
                    /*if (lengthV < beesController.beeSpeed || lengthV > beesController.beeSpeed + 1) {
                        veloVector.scale(beesController.beeSpeed/lengthV);
                    }*/
                    _bee.setVelocityY(veloVector.y);
                    _bee.setVelocityX(veloVector.x);
                    /*if (_bee.y < 0 || _bee.y + _bee.body.velocity.y < 0) _bee.setPosition(_bee.x, 50);*/
                }

                _bee.lastY = _bee.y;
                _bee.lastX = _bee.x;
            }
            
            if (_item) {
                audioController.clickSound.play();
                _item.itemObject.hit(_bee.beeObject);
            }
        });
    }
}

sceneRegistry.register(beesController);

// Items 
var itemsController = {
    maxItemsN: 20,
    items: [],
    preload: function (loader) {
        itemsController.items = [];
        itemsController.maxItemsN = 20;
        for (var i = 1; i<= 4; i++) 
            loader.load.image('florA' + i, 'assets/FlorMala1/FlorM' + i + '.png');
        for (var i = 1; i<= 8; i++) 
            loader.load.image('florB' + i, 'assets/FlorMala2/FlorM2_' + i + '.png');
        for (var i = 1; i<= 6; i++) 
            loader.load.image('florC' + i, 'assets/FlorMala3/FlorM3_' + i + '.png');
        for (var i = 1; i<= 3; i++) 
            loader.load.image('florNormal' + i, 'assets/FloresNormales/FlorNormal' + i + '.png');
        for (var i = 1; i<= 6; i++) 
            loader.load.image('maizT' + i, 'assets/MaizTransTriang1/MaizTransTriang' + i + '.png');
        /*for (var i = 1; i<= 6; i++) 
            loader.load.image('florD' + i, 'assets/FlorMalaTriang1/FlorMalaTriang' + i + '.png');*/
        for (var i = 1; i<= 6; i++) 
            loader.load.image('tomateT' + i, 'assets/TomateTransRombo1/TomateTransRombo' + i + '.png');
        for (var i = 1; i<= 4; i++) 
            loader.load.image('bomba' + i, 'assets/Bomba/bomba' + i + '.png');
        for (var i = 1; i<= 6; i++) 
            loader.load.image('explode' + i, 'assets/Explosion/Explosion' + i + '.png');
        loader.load.image('frozen1', 'assets/AbejitaCongelada.png');
        loader.load.image('contorno_hex', 'assets/contorno_hexagono.png');
        loader.load.image('contorno_hex_red', 'assets/contorno_hexagono_red.png');
        loader.load.image('contorno_cuadro', 'assets/contorno_cuadro.png');
        loader.load.image('contorno_cuadro_red', 'assets/contorno_cuadro_red.png');
        loader.load.image('contorno_rombo', 'assets/contorno_rombo.png');
        loader.load.image('contorno_rombo_red', 'assets/contorno_rombo_red.png');
        loader.load.image('contorno_tri', 'assets/contorno_tri.png');
        loader.load.image('contorno_tri_red', 'assets/contorno_tri_red.png');
        loader.load.image('contorno_penta', 'assets/contorno_penta.png');
        loader.load.image('contorno_penta_red', 'assets/contorno_penta_red.png');
    },
    create: function (scene) {
        scene.anims.create({
            key: 'florA',
            frames: ([1,2,3,4]).map((i) => ({key: "florA" + i, frame: null, duration:2})),
            frameRate: 15,
            repeat: -1
        });
        scene.anims.create({
            key: 'florB',
            frames: ([1,2,3,4,5,6,7,8]).map((i) => ({key: "florB" + i, frame: null, duration:2})),
            frameRate: 10,
            repeat: -1
        });
        scene.anims.create({
            key: 'florC',
            frames: ([1,2,3,4,5,6]).map((i) => ({key: "florC" + i, frame: null, duration:2})),
            frameRate: 10,
            repeat: -1
        });
       /* scene.anims.create({
            key: 'florD',
            frames: ([1,2,3,4,5,6]).map((i) => ({key: "florD" + i, frame: null, duration:2})),
            frameRate: 15,
            repeat: -1
        }); */
        scene.anims.create({
            key: 'tomateT',
            frames: ([1,2,3,4,5,6]).map((i) => ({key: "tomateT" + i, frame: null, duration:2})),
            frameRate: 10,
            repeat: -1
        });
        scene.anims.create({
            key: 'maizT',
            frames: ([1,2,3,4,5,6]).map((i) => ({key: "maizT" + i, frame: null, duration:2})),
            frameRate: 10,
            repeat: -1
        });
        scene.anims.create({
            key: 'bomba',
            frames: ([1,2,3,4]).map((i) => ({key: "bomba" + i, frame: null, duration:2})),
            frameRate: 50,
            repeat: -1
        });
        scene.anims.create({
            key: 'explode',
            frames: ([1,2,3,4,5,6]).map((i) => ({key: "explode" + i, frame: null, duration:1})),
            frameRate: 50,
            repeat: 1
        });
        
        itemsController.newRow = itemsController.__newRow.bind(null, scene, false);
        gameController.levelText.setText(gameController.level + "");
        
        for (var i = 8; i>=0; i--)
            itemsController.__newRow(scene,true);
    },
    __newRow: function (scene, ignoreLevel) {
        itemsController.items= itemsController.items.filter((i) => i.sprite.active);
        var lost = false;
        itemsController.items.forEach(function (i) {
            lost |= i.moveDown();
            if (i.type == "bomba") {
                i.hit(null, Math.round(i.initialN/7) );
            }
        });
        
        if (lost) gameController.gameOver(scene);
        
        if (!ignoreLevel) { 
            gameController.level += 1;
            gameController.levelText.setText(gameController.level + "");
            itemsController.maxItemsN = itemsController.maxItemsN>=500? 500: itemsController.maxItemsN+(gameController.level>100?2:1);
        }
        // min = 2
        // max = 10
        var nItems = 0;
        for (var i = 0; i< 13; i++) { 
            if (Phaser.Math.RND.rnd() < (Math.min(0.4, gameController.level/50)+0.4) && nItems < 11) { 
                nItems ++;
                var tipoItem = Phaser.Math.RND.pick(Item.itemsOptions);
                if (tipoItem == "bomba") {
                    if ( Math.round(gameController.level / 3) % 2 == 0)
                        continue;
                }
                if (tipoItem == "frozen") {
                    if (beesController.bees.length > 100 || Math.round(gameController.level / 3) % 2 == 1)
                        continue;
                }
                var n = Phaser.Math.RND.between(Math.round(itemsController.maxItemsN*0.5), itemsController.maxItemsN);
                itemsController.items.push(new Item(scene, 30 + 45*i, 0 + ((i%2)?30:0), 
                            gameController.catItems, tipoItem, n));
            }
        }
    },
}
sceneRegistry.register(itemsController);

var gameConfig = {
    type: Phaser.AUTO,
    width: 600,
    height: 800,
    backgroundColor: 0x333333,
    parent: 'output',
    physics: {
        default: 'matter',
        matter: {
            gravity: {
                y: 0
            },
            //debug: true
        }
    },
    pixelArt: false,
    scene: {
        preload: function () { sceneRegistry.preload(this); },
        create: function () { sceneRegistry.create(this); },
        update: function () { sceneRegistry.update(this); }
    }
};

Browser.init();

var game = new Phaser.Game(gameConfig);

var app = new Vue({
  el: '#game-gui',
  template: "#gui-template",
  data: {
    showContent: false,
    style: {
        height: 500,
        width: 500,
        left: 0
    },
    musicVolume: 5,
    allVolume: 10,
    homeScreen: true,
    gameOver: false,
    username: "",
    scoreSendState: null
  },
  watch: {
    musicVolume: function (v, o) {
        audioController.flightSong.setVolume(v/10);
    },
    allVolume: function (v, o) {
        game.scene.scenes[0].sound.setVolume(v/10);
    }
  },
  methods: {
    start: function () {
        this.homeScreen = false;
        beesController.uiStatus = "play";
    },
    restart: function () {
        beesController.timer.paused=true;
        beesController.timer.remove();
        beesController.timer.destroy();
        beesController.uiStatus = "play";
        game.destroy();
        document.querySelector("canvas").remove();
        game = new Phaser.Game(gameConfig);
        app.gameOver = false;
        app.scoreSendState = null;
        app.username = "";
    },
    ss: function () {
        this.scoreSendState = "sending";
        var xhrRequest = nanoajax.ajax({
            "url": " https://escapeteamgame.com/wp-json/stg/v1/scores",
            "headers": {
                "X-Stg-Key": axs
                },
            "method": "POST",
            "body": prepareBody([
                { key: "UserName", value: this.username},
                { key: "Level", value: ""},
                { key: "ScorePoints", value: String(gameController.level)},
                { key: "GameID", value: "InE_test"},
                { key: "ExtraInformation", value: "" }
            ])
        }, function (code, response, request) { 
            console.log(code, response, request);
            if (code ==200) {
                app.scoreSendState = "sent"; 
            }
            else {
                app.scoreSendState = "error"; 
            }
        });
    }
  }
});

function prepareBody(data){
    var output="";
    for (var i = 0; i < data.length; i++) {
        output += data[i].key + "=" + encodeURIComponent(data[i].value) + (i < data.length-1? "&" : "");
    }
    return output;
}
